#
#   Monogatari Engine Script Parser.
#   JS Sucks sometimes, especially it's related with big fat file refernece.
#
import re
import os
from os import path
from os import walk

MAIN_PATH = os.path.dirname(os.path.realpath(__file__))
INPUT_PATH = os.path.join(MAIN_PATH, "events")
OUTPUT_PATH = os.path.join(MAIN_PATH, "output")

def moveTo(tabs, line):
    return '{}"{}"\n'.format("\t"*tabs, "join " + line)

def rawText(tabs, line):
    return '{}{}\n'.format("\t"*tabs, line)

def choice(tabs, line):
    try:
        addedTabs = 0
        newBuffer = ("\t"*tabs + "{\n")
        newBuffer += ("\t"*(tabs+1) + "\"Choice\": {\n") 
        
        dnoArray = line.split(":")
        newBuffer += ("\t"*(tabs+2) + "\"Dialog\": \"" + dnoArray[0] + "\",\n")   
        
        optionsArray = dnoArray[1].split("|")
        for tndString in optionsArray:
            tndArray = tndString.split(",")
            newBuffer += ("\t"*(tabs+2) + "\"" + tndArray[1] + "\": {\n")   
            newBuffer += ("\t"*(tabs+3) + "\"Text\": \"" + tndArray[0] + "\",\n")   
            newBuffer += ("\t"*(tabs+3) + "\"Do\": \"jump " + tndArray[1] + "\"\n")   
            newBuffer += ("\t"*(tabs+2) + "},\n")   

        newBuffer += ("\t"*(tabs+1) + "}\n") 
        newBuffer += ("\t"*tabs + "},\n")

        return newBuffer
    except expression as identifier:
        print(identifier)
        return ""

SPECIAL_EXPRESSIONS = {
    "@": moveTo,
    "$": rawText,
    "*": choice,
}

eventStringBuffer = "let events = {\n"
indentLevel = 0

for (dirpath, dirnames, filenames) in walk(INPUT_PATH):
    for filename in filenames:
        with open(os.path.join(INPUT_PATH, filename), "r", encoding="utf8") as file:
            if file:
                #file must exist, there is no way that file is not actually exists.
                eventName = filename.split('.txt')[0]

                indentLevel = 1
                eventStringBuffer += ('{}"{}": [\n'.format('\t'*indentLevel, eventName))
                
                #include more shits to get more shit
                indentLevel = 2
                while True:
                    line = file.readline().replace('\n', '')
                    if not line:
                        break;

                    if (line[0] != "\n"):
                        specialExpressionFunc = SPECIAL_EXPRESSIONS.get(line[0])
                        
                        if (specialExpressionFunc):
                            eventStringBuffer += specialExpressionFunc(indentLevel, line[1:])
                        else:
                            eventStringBuffer += ('{}"{}",\n'.format('\t'*indentLevel, line))    
                    
                indentLevel = 1
                eventStringBuffer += ('{}],\n'.format("\t"*indentLevel))
        
eventStringBuffer += "}\n"

with open(os.path.join(OUTPUT_PATH, "output.txt"), "w", encoding="utf8") as file:
    if file:
        #if file is actually accessible.
        file.write(eventStringBuffer)
        print("SUCCESSFULLY WRITTEN THE RESULT.")